<?php require 'inc/db.php'; ?>

<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=KoHo|Niramit|Permanent+Marker" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cabin+Condensed:400,500|Imprima|Marmelad|Reem+Kufi"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    <link href="assets/css/style2.css" rel="stylesheet" />
    <link rel="icon" href="assets/images/fav.png">
    <title>This Next Year</title>
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <header class="header">
                    <div class="header-background kenburns-top"></div>
                    <div class="header-text content">
                        <h1 class="text-focus-in headerMain">This Next Year</h1>
                        <h3 class="text-focus-in headerParagraph">Write a letter to your future self, take action,<br>
                            receive it 365
                            days from today.</h3>
                        <a href="page2.php" type="button" id="nameMain" class="btn swing-in-top-fwd">START</a>
                    </div>
                </header>
            </div>
        </div>
    </div>
</body>

</html>