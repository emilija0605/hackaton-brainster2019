<?php 

require "inc/db.php";

// echo '<pre>'; var_dump($_POST); exit();

$user_id = 0;

if( isset($_POST['firstname']) && $_POST['firstname'] != '' && isset($_POST['email']) && $_POST['email'] != '') {
    
    $name = $_POST['firstname'];
    $email = $_POST['email'];

    // proveruva dali vo baza postoi korisnik so istiot mail
    $stmt = $myPDO->prepare("SELECT * FROM users WHERE email=:email");
    $stmt->execute(['email' => $email]); 
    $user = $stmt->fetch();

    if (!$user) {
        //vnesi u baza
        $data = [
            'name' => $name,
            'email' => $email,
        ];
        $sql = "INSERT INTO users (`name`, `email`) VALUES (:name, :email)";
        $x = $myPDO->prepare($sql);
        $x->execute($data);

         // Ako x e false znaci ne e vneseno uspeshno vo baza. Tuka treba da vratite napredhodna strana so greshka
        if (!$x) {
            header("Location: page2.php?error=true");
            die();
        } else {
            // setirame user_id da e id-to na posledniot vnesen user
            $user_id = $myPDO->lastInsertId();
        }

    } else {
        header("Location: index.php");
        die();
    }

}

if (isset($_POST['role']) && is_array($_POST['role']) && count($_POST['role']) > 0) {
    $roleArrPost = $_POST['role'];
}

if( isset($_POST['name1']) && !is_numeric($_POST['name1']) && $_POST['name1'] != '') {

    $role = $_POST['name1'];
    //vnesi u baza
    $data = [
        'role' => $role,
    ];
    $sql = "INSERT INTO roles (`role`) VALUES (:role)";
    $x = $myPDO->prepare($sql);
    $x->execute($data);

    if (!$x) {
        header("Location: page2.php?error=true");
        die();
    }

    $name1LastInsertId = $myPDO->lastInsertId(); 

    $key1 = array_search($role, $roleArrPost);
    $roleArrPost[$key1] = $name1LastInsertId;
}

if (isset($_POST['currently']) && is_array($_POST['currently']) && count($_POST['currently']) > 0) {
    $currentlyArrPost = $_POST['currently'];
}

if( isset($_POST['name2']) && !is_numeric($_POST['name2']) && $_POST['name2'] != '') {

    $currently = $_POST['name2'];
    //vnesi u baza
    $data = [
        'currently' => $currently,
    ];
    $sql = "INSERT INTO currently_at (`currently`) VALUES (:currently);";
    $x = $myPDO->prepare($sql);
    $x->execute($data);

    if (!$x) {
        header("Location: page2.php?error=true");
        die();
    }

    $name2LastInsertId = $myPDO->lastInsertId(); 

    $key2 = array_search($currently, $currentlyArrPost);
    $currentlyArrPost[$key2] = $name2LastInsertId;

}

if (isset($_POST['interest']) && is_array($_POST['interest']) && count($_POST['interest']) > 0) {
    $interestArrPost = $_POST['interest'];
}

if( isset($_POST['name3']) && !is_numeric($_POST['name3']) && $_POST['name3'] != '') {

    $interest = $_POST['name3'];
    //vnesi u baza
    $data = [
        'interest' => $interest,
    ];
    $sql = "INSERT INTO interests (`interest`) VALUES (:interest)";
    $x = $myPDO->prepare($sql);
    $x->execute($data);

    if (!$x) {
        header("Location: page2.php?error=true");
        die();
    }

    $name3LastInsertId = $myPDO->lastInsertId(); 

    $key3 = array_search($interest, $interestArrPost);
    $interestArrPost[$key3] = $name3LastInsertId;

}

if( isset($_POST['plan']) && $_POST['plan'] != '' && isset($_POST['health']) && $_POST['health'] != '') {

    $text = $_POST['plan'];
    $health_id = $_POST['health'];
    //vnesi u baza
    $data = [
        'text' => $text,
        'user_id' => $user_id,
        'health_id' => $health_id,
    ];
    $sql = "INSERT INTO user_health (`text`, `user_id`,	`health_id`) 
    VALUES (:text, :user_id, :health_id)";
    $x = $myPDO->prepare($sql);
    $x->execute($data);

    if (!$x) {
        header("Location: page2.php?error=true");
        die();
    }
}

if( isset($_POST['personal']) && $_POST['personal'] != '' && isset($_POST['interest']) && is_array($_POST['interest'])) {

    foreach($interestArrPost as $key => $value) {
        $text = $_POST['personal'];
        $interest_id = $value;
        //vnesi u baza
        $data = [
            'text' => $text,
            'user_id' => $user_id,
            'interest_id' => $interest_id,
        ];
        $sql = "INSERT INTO user_interest (`text`, `user_id`, `interest_id`) 
        VALUES (:text, :user_id, :interest_id)";
        $x = $myPDO->prepare($sql);
        $x->execute($data);

    }

    if (!$x) {
        header("Location: page2.php?error=true");
        die();
    }
}

if( isset($_POST['relation']) && $_POST['relation'] != '' && isset($_POST['relationship']) && $_POST['relationship'] != '') {

    $text = $_POST['relation'];
    $relationship_id = $_POST['relationship'];
    //vnesi u baza
    $data = [
        'text' => $text,
        'user_id' => $user_id,
        'relationship_id' => $relationship_id,
    ];
    $sql = "INSERT INTO user_relationship (`text`, `user_id`, `relationship_id`) 
    VALUES (:text, :user_id, :relationship_id)";
    $x = $myPDO->prepare($sql);
    $x->execute($data);

    if (!$x) {
        header("Location: page2.php?error=true");
        die();
    }
}

if( isset($_POST['carieer']) && $_POST['carieer'] != '' && isset($_POST['role']) && is_array($_POST['role']) && isset($_POST['currently']) && is_array($_POST['currently']) && isset($_POST['feel']) && $_POST['feel'] != '') {

    $text = $_POST['carieer'];
    $feeling_id = $_POST['feel'];

    if(count($roleArrPost) == count($currentlyArrPost)) {
    
        foreach ($roleArrPost as $key => $role) {

            foreach ($currentlyArrPost as $key => $currently) {
                $role_id = $role;
                $currently_at_id = $currently;
                //vnesi u baza
                $data = [
                    'text' => $text,
                    'user_id' => $user_id,
                    'role_id' => $role_id,
                    'currently_at_id' => $currently_at_id,
                    'feeling_id' => $feeling_id,
                ];
                $sql = "INSERT INTO career (`text`,	`user_id`, `role_id`, `currently_at_id`, `feeling_id`) 
                VALUES (:text, :user_id, :role_id, :currently_at_id, :feeling_id)";
                $x = $myPDO->prepare($sql);
                $x->execute($data);

            }
        }

    } else if(count($roleArrPost) == 1 && count($currentlyArrPost) == 2) {

        $role_id = $roleArrPost[0];

        foreach ($currentlyArrPost as $key => $currently) {
            $currently_at_id = $currently;
            //vnesi u baza
            $data = [
                'text' => $text,
                'user_id' => $user_id,
                'role_id' => $role_id,
                'currently_at_id' => $currently_at_id,
                'feeling_id' => $feeling_id,
            ];
            $sql = "INSERT INTO career (`text`,	`user_id`, `role_id`, `currently_at_id`, `feeling_id`) 
            VALUES (:text, :user_id, :role_id, :currently_at_id, :feeling_id)";
            $x = $myPDO->prepare($sql);
            $x->execute($data);

        }

    } else if(count($roleArrPost) == 2 && count($currentlyArrPost) == 1) {

        $currently_at_id = $currentlyArrPost[0];

        foreach ($roleArrPost as $key => $role) {
            $role_id = $role;
            //vnesi u baza
            $data = [
                'text' => $text,
                'user_id' => $user_id,
                'role_id' => $role_id,
                'currently_at_id' => $currently_at_id,
                'feeling_id' => $feeling_id,
            ];
            $sql = "INSERT INTO career (`text`,	`user_id`, `role_id`, `currently_at_id`, `feeling_id`) 
            VALUES (:text, :user_id, :role_id, :currently_at_id, :feeling_id)";
            $x = $myPDO->prepare($sql);
            $x->execute($data);

        }
       
    }

    if (!$x) {
        header("Location: page2.php?error=true");
        die();
    }
}

echo "<img src='assets/images/thankyou.png'/>";