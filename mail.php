<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
// Load Composer's autoloader
require 'vendor/autoload.php';
// Instantiation and passing `true` enables exceptions

function sendMail($array) {
    $mail = new PHPMailer(true);
    try {
        //Server settings
        $mail->SMTPDebug = 1;                                       // Enable verbose debug output
        $mail->isSMTP();                                            // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'eristova5695@gmail.com';                     // SMTP username
        $mail->Password   = 'emilija0605';                               // SMTP password
        $mail->SMTPSecure = 'ssl';                                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port       = 465;                                    // TCP port to connect to
        //Recipients
        $mail->setFrom('eristova5695@gmail.com');
        $mail->addAddress($array['email']);               // Name is optional
    
        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'This Next Year';
        $mail->Body    = 
            'Hey ' . $array['name'] . '! <br>
            Its been a year since you first took our quiz. 365 days! Time sure flies, heh?<br><br>
            
            You were a <b>' .  $array['role'] . '</b> at <b>' .  $array['currently_at'] . '</b> back then. You were feeling like <b>' . $array['feeling'] . '</b> at that time, and left this 
            comment to your future self: <i>' . $array['career_text'] . '</i><br><br>
            
            Did any of that change?<br><br>
            
            You described your health as <b>' . $array['health'] . '</b>, but we truly hope you are in your best shape right now! <br>
            Last year you said: <i>' . $array['health_text'] . '</i><br><br>
            
            Everybody got their interests in something, and yours was <b>' . $array['interest'] . '</b>. We hope you had many more during
            this period, but we hope that you still remember this words as well: <i>' . $array['interest_text'] . '</i><br><br>
            
            Last but not least, you described your relationship like <b>' . $array['relationship'] . '</b>. <br>
            We hope things continued to get better every day! <br>
            Your last comment about it was: <i>' . $array['relationship_text'] . '</i> <br><br>
            
            So... How did you do?<br>
            Let us know!
            
            Sincerely, 
            Brainster team!';
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        echo 'Message has been sent';
        
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }
}

