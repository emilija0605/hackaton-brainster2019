<?php

function checkUserEmail($email) {
    require "inc/db.php";

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return ['success' => false, 'message'=> 'Please enter a validate email.'];
    }

    // proveruva dali vo baza postoi korisnik so istiot mail
    $stmt = $myPDO->prepare("SELECT * FROM users WHERE email=:email");
    $stmt->execute(['email' => $email]); 
    $user = $stmt->fetch();
    
    if ($user) {
        return ['success' => false, 'message'=> 'Duplicate user email.'];
    }

    return ['success' => true, 'message'=> 'All good.'];
}

if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'validate-email':
            $email = isset($_POST['email']) ? $_POST['email'] : '';
            echo json_encode(checkUserEmail($email));
            break;
        default:
            break;
    }
}