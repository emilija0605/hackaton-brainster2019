<?php
/**
 * This files sends email to all users that took the questionaire a year ago.
 * In order to fire the email, this file has to be executed after 00:01 every day.
 * To achieve so a Cron Job is the suggested tool to use. 
 */
require 'inc/db.php';
require 'mail.php';

$curentdate = date("Y-m-d", time());
$startdate = strtotime($curentdate); 
$date = strtotime('-1 year', $startdate);
$enddate = date("Y-m-d", $date);

$users = $myPDO->query("SELECT * FROM users WHERE date LIKE '%$enddate%';")->fetchAll();

$usersAnswers = [];
foreach ($users as $user) {
    $id = $user['id'];
    $name = $user['name'];
    $email = $user['email'];

    $career_text = $feeling_value = $health_value = $health_text =  
    $interest_text = $relationship_text = $relationship_value = '';

    $role_values = $currently_at_values = $interest_values = [];

    $career = $myPDO->query("SELECT * FROM career WHERE user_id = '$id';")->fetchAll();
    foreach ($career as $value) {
        $career_text =  $value['text'];
    }
    
    $health = $myPDO->query("SELECT * FROM user_health WHERE user_id = '$id';")->fetchAll();
    foreach ($health as $value) {
        $health_text =  $value['text'];
    }

    $interest = $myPDO->query("SELECT * FROM user_interest WHERE user_id = '$id';")->fetchAll();
    foreach ($interest as $value) {
        $interest_text =  $value['text'];
    }

    $relationship = $myPDO->query("SELECT * FROM user_relationship WHERE user_id = '$id';")->fetchAll();
    foreach ($relationship as $value) {
        $relationship_text =  $value['text'];
    }

    $role = $myPDO->query("
        SELECT roles.role 
        FROM career 
        INNER JOIN roles ON career.role_id = roles.id 
        WHERE user_id = '$id';
    ")->fetchAll((PDO::FETCH_UNIQUE));
    foreach ($role as $key => $value) {
        $role_values[] = $key;
    }

    $currently_at = $myPDO->query("
        SELECT currently_at.currently 
        FROM career 
        INNER JOIN currently_at ON career.currently_at_id = currently_at.id 
        WHERE user_id = '$id';
    ")->fetchAll((PDO::FETCH_UNIQUE));
    foreach ($currently_at as $key => $value) {
        $currently_at_values[] = $key;
    }

    $feeling = $myPDO->query("
        SELECT feeling.feel 
        FROM career 
        INNER JOIN feeling ON career.feeling_id = feeling.id 
        WHERE user_id = '$id';
    ")->fetchAll();
    foreach ($feeling as $value) {
        $feeling_value = $value['feel'];
    }

    $health1 = $myPDO->query("
        SELECT health.wellness 
        FROM user_health 
        INNER JOIN health ON user_health.health_id = health.id 
        WHERE user_id = '$id';
    ")->fetchAll();
    foreach ($health1 as $value) {
        $health_value = $value['wellness'];
    }

    $interest1 = $myPDO->query("
        SELECT interests.interest 
        FROM user_interest 
        INNER JOIN interests ON user_interest.interest_id = interests.id
        WHERE user_id = '$id';
    ")->fetchAll();
    foreach ($interest1 as $value) {
        $interest_values[] = $value['interest'];
    }

    $relationship1 = $myPDO->query("
        SELECT relationships.skill 
        FROM user_relationship 
        INNER JOIN relationships ON user_relationship.relationship_id = relationships.id 
        WHERE user_id = '$id';
    ")->fetchAll();
    foreach ($relationship1 as $value) {
        $relationship_value = $value['skill'];
    }

    $usersAnswers[] = [
        'user_id' => $id, 
        'name' => $name, 
        'email' => $email, 
        'career_text' => $career_text,
        'role' => implode(", ", $role_values),
        'currently_at' => implode(", ", $currently_at_values),
        'feeling' => $feeling_value,
        'health' => $health_value,
        'health_text' => $health_text,
        'interest' => implode(", ", $interest_values),
        'interest_text' => $interest_text,
        'relationship_text' => $relationship_text,
        'relationship' => $relationship_value,
    ];
}

 
foreach ($usersAnswers as $userArr) {
    sendMail($userArr);
}
